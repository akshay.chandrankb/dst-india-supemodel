/*********************************************************************************************
 *   Copyright (c) <2020>, <Santosh Ansumali@JNCASR>                                         *
 *   All rights reserved.                                                                    *
 *   Redistribution and use in source and binary forms, with or without modification, are    *
 *   permitted provided that the following conditions are met:                               *
 *                                                                                           *
 *    1. Redistributions of source code must retain the above copyright notice, this list of *
 *       conditions and the following disclaimer.                                            *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list *
 *       of conditions and the following disclaimer in the documentation and/or other        *
 *       materials provided with the distribution.                                           *
 *    3. Neither the name of the <JNCASR> nor the names of its contributors may be used to   *
 *       endorse or promote products derived from this software without specific prior       *
 *       written permission.                                                                 *
 *                                                                                           *
 *       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND     *
 *       ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED       *
 *       WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  *
 *       IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,    *
 *       INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,      *
 *       BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,       *
 *       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 *       LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE     *
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED   *
 *       OF THE POSSIBILITY OF SUCH DAMAGE.                                                  *
 *                                                                                           *
 *       Suggestions:          ansumali@jncasr.ac.in                                         *
 *       Bugs:                 ansumali@jncasr.ac.in                                         *
 *                                                                                           *
 *********************************************************************************************/


/*********************************************************************************************

 *  @Authors: Shaurya Kaushal, Akshay Chandran and Santosh Ansumali                                            *

 *********************************************************************************************/

#include<iostream>
#include<fstream>
#include<stdio.h>
#include<unistd.h>
#include<cstdlib>
#include<cmath>
#include<iomanip>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<algorithm>
#include<string.h>
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"


using namespace std;

void betaGnuplotPrint()
{
      FILE *pipe = popen("gnuplot -persist","w");
      fprintf(pipe,"\n");

      fprintf(pipe,"reset\n");
      fprintf(pipe,"#set terminal pngcairo size 1200,800\n");
      fprintf(pipe,"set terminal postscript eps size 9,5 enhanced color\n");
      fprintf(pipe,"set output \'./I_vs_time.eps\'\n");
      fprintf(pipe,"set bmargin 8\n");
      fprintf(pipe,"set lmargin 15\n");
      fprintf(pipe,"set encoding iso_8859_1\n");
      fprintf(pipe,"set key bottom right\n");
      fprintf(pipe,"set key opaque\n");
      fprintf(pipe,"set key font \"Helvetica,42\"\n");
      fprintf(pipe,"set key spacing 1.5\n");
      fprintf(pipe,"#set title \"Armenia : {/Symbol g}^{-1} Estimation\" font \"Helvetica,32\"\n");
      fprintf(pipe,"set xrange [0:100] noreverse writeback\n");
      fprintf(pipe,"set xtics ( \"0\" 0, \"10\" 10, \"20\" 20, \"30\" 30, \"40\" 40, \"50\" 50, \"60\" 60, \"70\" 70, \"80\" 80, \"90\" 90, \"100\" 100) offset 0,-1.5  font \"Helvetica,30\"\n");
      fprintf(pipe,"set ytics (  \"10\" 2.3, \"10^2\" 4.6, \"10^3\" 6.9, \"10^4\" 9.21, \"10^5\" 11.51, \"10^6\" 13.81) offset 0,-1.5  font \"Helvetica,30\"\n");
      fprintf(pipe,"set yrange [0:15] writeback\n");
      fprintf(pipe,"set xlabel \"Time (in days)\" offset 0,-3 font \"Helvetica,42\"\n");
      fprintf(pipe,"set ylabel \"I (Active infections)\" offset -7 font \"Helvetica,42\"\n");
      fprintf(pipe,"#set grid ytics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
      fprintf(pipe,"#set grid xtics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
      fprintf(pipe,"set datafile separator \",\"\n");

      fprintf(pipe,"gamma1 = 0.030894017154711\n");
      fprintf(pipe,"gamma2 = 0.0768658655312062\n");
      fprintf(pipe,"beta1  = 0.2\n");
      fprintf(pipe,"beta2  = 0.2\n");
      fprintf(pipe,"a1 = 1\n");
      fprintf(pipe,"a2 = 1\n");

      fprintf(pipe,"h1(x) = a1+ ((beta1-gamma1)*x)\n");
      fprintf(pipe,"h2(x) = a2+ ((beta2-gamma2)*x)\n");


      fprintf(pipe,"fit h1(x) \'< cat ./output/I_run_av_7_fit1.data\'  using ($1):(log($2)) via a1,beta1\n");
      fprintf(pipe,"fit h2(x) \'< cat ./output/I_run_av_7_fit2.data\'  using ($1):(log($2)) via a2,beta2\n");

      fprintf(pipe,"set print \"./parameters.txt\" append\n");
      fprintf(pipe,"print beta1\n");
      fprintf(pipe,"print beta2\n");
      fprintf(pipe,"set print\n");

      // fprintf(pipe,"plot      \"./beta/I_run_av_7.data\"    every 2          using  ($1):(log($2))       with points pt 6 ps 3 lw 4 lc 8 dt 1  title \'Real data\' ,\\\n");
      // fprintf(pipe,"         h1(x)                with lines lw 6 lc 8 dt 1 title \"{/Symbol b_0} = 0.26   \" ,\\n");
      // fprintf(pipe,"         h2(x)                with lines lw 6 lc 8 dt 2 title \"{/Symbol b_1} = 0.09   \"");

      fclose(pipe);

      fflush(pipe);
}

void deltaGnuplotPrint()
{

        FILE *pipe = popen("gnuplot -persist","w");
        fprintf(pipe,"\n");

        fprintf(pipe,"reset\n");
        fprintf(pipe,"#set terminal pngcairo size 1200,800\n");
        fprintf(pipe,"set terminal postscript eps size 9,5 enhanced color\n");
        fprintf(pipe,"set output \'./Idot_plus_gammaI_vs_time.eps\'\n");
        fprintf(pipe,"set bmargin 8\n");
        fprintf(pipe,"set lmargin 15\n");
        fprintf(pipe,"set encoding iso_8859_1\n");
        fprintf(pipe,"set key bottom right\n");
        fprintf(pipe,"set key font \"Helvetica,42\"\n");
        fprintf(pipe,"set key spacing 2\n");
        fprintf(pipe,"#set title \"Armenia : {/Symbol g}^{-1} Estimation\" font \"Helvetica,32\"\n");
        fprintf(pipe,"set xrange [0:100] noreverse writeback\n");
        fprintf(pipe,"set xtics ( \"0\" 0, \"10\" 10, \"20\" 20, \"30\" 30, \"40\" 40, \"50\" 50, \"60\" 60, \"70\" 70, \"80\" 80, \"90\" 90, \"100\" 100) offset 0,-1.5  font \"Helvetica,30\"\n");
        fprintf(pipe,"set ytics (  \"10\" 2.3, \"10^2\" 4.6, \"10^3\" 6.9, \"10^4\" 9.21, \"10^5\" 11.51, \"10^6\" 13.81) offset 0,-1.5  font \"Helvetica,30\"\n");
        fprintf(pipe,"set yrange [0:10] writeback\n");
        fprintf(pipe,"set xlabel \"Time (in days)\" offset 0,-3.5 font \"Helvetica,42\"\n");
        fprintf(pipe,"set ylabel \"  dI/dt + {/Symbol g}I \" offset -7 font \"Helvetica,42\"\n");
        fprintf(pipe,"#set grid ytics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
        fprintf(pipe,"#set grid xtics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
        fprintf(pipe,"set datafile separator \",\"\n");

        // fprintf(pipe,"set arrow from 9, graph 0 to 9,   graph 1 nohead lw 3 dt 2 lc rgb \"black\" filled\n");
        // fprintf(pipe,"set arrow from 30, graph 0 to 30, graph 1 nohead lw 3 dt 2 lc rgb \"black\" filled\n");
        // fprintf(pipe,"set arrow from 49, graph 0 to 49, graph 1 nohead lw 3 dt 2 lc rgb \"black\" filled\n");
        // fprintf(pipe,"set arrow from 67, graph 0 to 67, graph 1 nohead lw 3 dt 2 lc rgb \"black\" filled\n");

        fprintf(pipe,"set label \"L1\" textcolor rgb \"black\" at 9.5  ,9 font \"Helvetica,36\"\n");
        fprintf(pipe,"set label \"L2\" textcolor rgb \"black\" at 30.5 ,9 font \"Helvetica,36\"\n");
        fprintf(pipe,"set label \"L3\" textcolor rgb \"black\" at 49.5 ,9 font \"Helvetica,36\"\n");
        fprintf(pipe,"set label \"L4\" textcolor rgb \"black\" at 67.5 ,9 font \"Helvetica,36\"\n");

        fprintf(pipe,"gamma = 0.034\n");
        fprintf(pipe,"delta = 0.01\n");

        fprintf(pipe,"a = 1\n");

        fprintf(pipe,"h1(x) = a+ (-(delta+gamma)*x)\n");


        fprintf(pipe,"fit h1(x) \'< cat ./output/I_run_av_7_fit1.data\'  using ($1):(log($3+$2*gamma)) via a,delta\n");

        fprintf(pipe,"set print \"./parameters.txt\" append\n");
        fprintf(pipe,"print delta\n");
        fprintf(pipe,"set print\n");




        // fprintf(pipe,"plot       \"./delta/I_run_av_7.data\"             using  ($1):(log($3+$2*gamma))       with points pt 6 ps 2 lw 4 lc 8 dt 1  title \'\' ,\\\n");
        // fprintf(pipe,"         h1(x)                with lines lw 4 lc 8 dt 1 title \"{/Symbol d} =  0.05\"");

        fclose(pipe);

        fflush(pipe);





}

void gammaGnuplotPrint()
{
      FILE *pipe = popen("gnuplot -persist","w");
      fprintf(pipe,"\n");

      fprintf(pipe,"reset\n");
      fprintf(pipe,"#set terminal pngcairo size 1200,800\n");
      fprintf(pipe,"set terminal postscript eps size 7,5 enhanced color\n");
      fprintf(pipe,"set output \'./gamma.eps\'\n");
      fprintf(pipe,"set bmargin 8\n");
      fprintf(pipe,"set lmargin 15\n");
      fprintf(pipe,"set encoding iso_8859_1\n");
      fprintf(pipe,"set key top left\n");
      fprintf(pipe,"set key font \"Helvetica,42\"\n");
      fprintf(pipe,"set key spacing 1.5\n");
      fprintf(pipe,"#set title \"Armenia : {/Symbol g}^{-1} Estimation\" font \"Helvetica,32\"\n");
      fprintf(pipe,"#set xrange [0:5e5] noreverse writeback\n");
      fprintf(pipe,"set ytics ( \"0\" 0, \"2e4\" 20000, \"4e4\" 40000, \"6e4\" 60000, \"8e4\" 80000, \"1e5\" 100000) offset 0,-1  font \"Helvetica,28\" \n");
      fprintf(pipe,"set xtics ( \"0\" 0, \"6e5\" 600000, \"2e5\" 200000, \"8e5\" 800000, \"4e5\" 400000, \"10e5\" 1000000, \"12e5\" 1200000) offset 0,-1  font \"Helvetica,28\" \n");

      fprintf(pipe,"#set yrange [0:150000] writeback\n");
      fprintf(pipe,"set xlabel \"{/Symbol \362} I dt\" offset 0,-3 font \"Helvetica,42\"\n");
      fprintf(pipe,"set ylabel \"R_I (Recovery + Death)\" offset -5 font \"Helvetica,42\"\n");
      fprintf(pipe,"#set grid ytics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
      fprintf(pipe,"#set grid xtics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
      fprintf(pipe,"set datafile separator \",\"\n");


      fprintf(pipe,"gamma1 = 0.04\n");
      fprintf(pipe,"gamma2 = 0.04\n");
      fprintf(pipe,"c1 = 100\n");
      fprintf(pipe,"c2 = 100\n");

      fprintf(pipe,"h1(x) = gamma1* x + c1\n");
      fprintf(pipe,"h2(x) = gamma2* x + c2\n");


      fprintf(pipe,"fit h1(x) \'< cat ./output/R1dot_I_run_av_7_int2_fit1.data\'  using 2:1 via gamma1,c1\n");
      fprintf(pipe,"fit h2(x) \'< cat ./output/R1dot_I_run_av_7_int2_fit2.data\'  using 2:1 via gamma2,c2\n");

      fprintf(pipe,"set print \"./parameters.txt\" append\n");
      fprintf(pipe,"print gamma1\n");
      fprintf(pipe,"print gamma2\n");
      fprintf(pipe,"set print\n");


      // fprintf(pipe,"plot      \"./output/R1dot_I_run_av_7_int2.data\"    every 1              using  2:1       with points pt 6 ps 2 lw 4 lc 8 title \'Real data\'   \n");
      // fprintf(pipe,"         h1(x)                with lines lw 6 lc 8  \n");
      // fprintf(pipe,"         h2(x)                with lines lw 4 lc 7 title \'\' \n");

      fclose(pipe);

      fflush(pipe);
}

void gammaDGnuplotPrint()
{
      FILE *pipe = popen("gnuplot -persist","w");
      fprintf(pipe,"\n");

      fprintf(pipe,"reset\n");
      fprintf(pipe,"#set terminal pngcairo size 1200,800\n");
      fprintf(pipe,"set terminal postscript eps size 7,5 enhanced color\n");
      fprintf(pipe,"set output \'./gammaD.eps\'\n");
      fprintf(pipe,"set bmargin 8\n");
      fprintf(pipe,"set lmargin 15\n");
      fprintf(pipe,"set encoding iso_8859_1\n");
      fprintf(pipe,"set key top left\n");
      fprintf(pipe,"set key font \"Helvetica,42\"\n");
      fprintf(pipe,"set key spacing 1.5\n");
      fprintf(pipe,"#set title \"Armenia : {/Symbol g}^{-1} Estimation\" font \"Helvetica,32\"\n");
      fprintf(pipe,"set xrange [0:5e5] noreverse writeback\n");
      fprintf(pipe,"set ytics ( \"0\" 0, \"5e3\" 5000, \"10e3\" 10000, \"15e3\" 15000, \"20e3\" 20000) offset 0,-1  font \"Helvetica,28\"\n");
      fprintf(pipe,"set xtics ( \"0\" 0, \"1e5\" 100000, \"2e5\" 200000, \"3e5\" 300000, \"4e5\" 400000, \"5e5\" 500000, \"6e6\" 6000000,\"7e6\" 7000000) offset 0,-1  font \"Helvetica,28\"\n");

      fprintf(pipe,"#set yrange [0:150000] writeback\n");
      fprintf(pipe,"set xlabel \"{/Symbol \362} I dt\" offset 0,-3 font \"Helvetica,42\"\n");
      fprintf(pipe,"set ylabel \"R_I (Recovery + Death)\" offset -5 font \"Helvetica,42\"\n");
      fprintf(pipe,"#set grid ytics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
      fprintf(pipe,"#set grid xtics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
      fprintf(pipe,"set datafile separator \",\"\n");


      fprintf(pipe,"gamma1 = 0.04\n");
      fprintf(pipe,"gamma2 = 0.04\n");
      fprintf(pipe,"c1 = 100\n");
      fprintf(pipe,"c2 = 100\n");

      fprintf(pipe,"h1(x) = gamma1* x + c1\n");
      fprintf(pipe,"h2(x) = gamma2* x + c2\n");


      fprintf(pipe,"fit h1(x) \'< cat ./output/R1dot_I_run_av_7_int3_fit1.data\'  using 2:1 via gamma1,c1\n");
      fprintf(pipe,"#fit h2(x) \'< cat ./output/R1dot_I_run_av_7_int3_fit2.data\'  using 1:2 via gammainv2,c2\n");

      fprintf(pipe,"set print \"./parameters.txt\" append\n");
      fprintf(pipe,"print gamma1\n");
      fprintf(pipe,"set print\n");

      // fprintf(pipe,"plot      \"./gammaD/R1dot_I_run_av_7_int.data\"    every 1              using  2:1       with points pt 6 ps 2 lw 4 lc 8 dt 1  title \'Real data\' ,\\n");
      // fprintf(pipe,"h1(x)                with lines lw 6 lc 8 dt 1 title \"{/Symbol g} = 0.032\"\n");
      // fprintf(pipe,"#          h2(x)                with lines lw 4 lc 7 dt 1 title \"{/Symbol g_2}^{-1} =  112 \"");

      fclose(pipe);

      fflush(pipe);
}

void gammaAGnuplotPrint()
{
      FILE *pipe = popen("gnuplot -persist","w");
      fprintf(pipe,"\n");

      fprintf(pipe,"reset\n");
      fprintf(pipe,"set terminal pngcairo size 1200,800 \n");
      fprintf(pipe,"#set terminal postscript eps size 7,5 enhanced color\n");
      fprintf(pipe,"set output \'./gammaA.png\'\n");
      fprintf(pipe,"set bmargin 8\n");
      fprintf(pipe,"set lmargin 15\n");
      fprintf(pipe,"set encoding iso_8859_1 \n");
      fprintf(pipe,"set key top left \n");
      fprintf(pipe,"set key font \"Helvetica,42\" \n");
      fprintf(pipe,"set key spacing 1.5\n");
      fprintf(pipe,"#set title \"Armenia : {/Symbol g}^{-1} Estimation\" font \"Helvetica,32\"\n");
      fprintf(pipe,"#set xrange [0:5e5] noreverse writeback\n");
      fprintf(pipe,"set ytics ( \"0\" 0, \"5e3\" 5000, \"10e3\" 10000, \"15e3\" 15000, \"20e3\" 20000) offset 0,-1  font \"Helvetica,28\"\n");
      fprintf(pipe,"set xtics ( \"0\" 0, \"1e5\" 100000, \"2e5\" 200000, \"3e5\" 300000, \"4e5\" 400000, \"5e5\" 500000, \"6e6\" 6000000,\"7e6\" 7000000) offset 0,-1  font \"Helvetica,28\"\n");

      fprintf(pipe,"#set yrange [0:150000] writeback\n");
      fprintf(pipe,"set xlabel \"t\" offset 0,-3 font \"Helvetica,42\"\n");
      fprintf(pipe,"set ylabel \"gammaA\" offset -5 font \"Helvetica,42\"\n");
      fprintf(pipe,"#set grid ytics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
      fprintf(pipe,"#set grid xtics lt 1 lw 1 lc rgb \"#bbbbbb\"\n");
      fprintf(pipe,"set datafile separator \",\" \n");


      fprintf(pipe,"delta = 0.05\n");
      fprintf(pipe,"beta1  = 0.09\n");
      fprintf(pipe,"beta2  = 0.26\n");

      fprintf(pipe,"m = 0.08 \n");
      fprintf(pipe,"c = 0.01\n");

      fprintf(pipe,"m2 = 0.08 \n");
      fprintf(pipe,"c2 = 0.01\n");

      fprintf(pipe,"h1(x) = m*x + c\n");

      fprintf(pipe,"fit h1(x) \'< cat ./output/I_run_av_7_fit1.data\'  using 1:($4 + delta*$1 + $5) via m,c\n");
      fprintf(pipe,"#fit h2(x) \'< cat ./output/I_run_av_7_fit2.data\'  using 1:($4 + delta*$1 + $5) via m2,c2\n");

      fprintf(pipe,"set print \"./parameters.txt\" append\n");
      fprintf(pipe,"print m\n");
      fprintf(pipe,"set print\n");

      // fprintf(pipe,"plot      \"./gammaA/I_run_av_7.data\"    every 1              using  1:($4 + delta*$1 + $5)     with points pt 6 ps 2 lw 4 lc 8 dt 1  title \'Real data\' ,\\n");
      // fprintf(pipe,"h1(x)                with lines lw 6 lc 8 dt 1 title \"{/Symbol g}_A = 0.09\"\n");
      // fprintf(pipe,"#          h2(x)                with lines lw 4 lc 7 dt 1 title \"{/Symbol g_2}^{-1} =  112 \"\n");

      fclose(pipe);

      fflush(pipe);
}

void betaGen(int cutoff)
{
            std::ifstream file1("./scrapedData/I.data");

            int t0int = 0;


            //////////////////////////////////////////////////////////////////////////////////////////////////

            int val(0), rows(0), cols(0), numItems(0);
            while( file1.peek() != '\n' && file1 >> val )
            {
            std::cout << val << ' ';
            ++numItems;
            }
            cols = numItems;// # of columns found

            std::cout << '\n';
            while( file1 >> val )
            {
            ++numItems;
            std::cout << val << ' ';
            if( numItems%cols == 0 ) std::cout << '\n';
            }

            if( numItems > 0 )// got data!
            {
            rows = numItems/cols;
            std::cout << "rows = " << rows << ", cols = " << cols << '\n';
            }
            else// didn't get any data
            std::cout << "data reading failed\n";

            int num_days = rows;

            //////////////////////////////////////////////////////////////////////////////////////////////////





            std::ifstream file ("./scrapedData/R1dot.data");
            std::ifstream file3("./scrapedData/I.data");
            if(file.is_open() && file3.is_open())
            {
            double  cases[num_days];
            double  reco[num_days];

            for(int i = 0; i < num_days; ++i)
            {
            //file >> reco[i];
            file3>> cases[i];

            }






            int p  = 7;      //running average days
            int np = num_days-(p-1);

            int cases_run[np];
            int reco_run[np];
            int total_cases[num_days];
            int total_reco[num_days];

            for(int j=0; j<np; j++){
            cases_run[j]   = 0.0;
            reco_run[j]    = 0.0;
            total_cases[j] = 0.0;
            total_reco[j]  = 0.0;
            }





            for(int j=0; j<np;j++){
            for(int i=j+p-1; i>=j; i--){
            cases_run[j] =  cases_run[j] + cases[i] ;
            reco_run[j]  =   reco_run[j] + reco[i]  ;

            } }

            total_cases[0]  = cases_run[0] ;
            total_reco[0]   = reco_run[0]  ;

            for(int j=1; j<np; j++){
            if(j>=t0int){
            total_cases[j]  = total_cases[j-1]  + cases_run[j] ;
            total_reco[j]   = total_reco[j-1]   +  reco_run[j] ;
            }
            }



            //Printing

            //*****************************************************************************************************************************
            /*
            *  std::ofstream file2;
            char fileName1[150];
            sprintf(fileName1,"R1dot_I_run_av_%d_int.data",p);
            file2.open(fileName1);

            for (int j=t0int;j<np;j++){


            //file2 <<reco_run[j]/p<<","<<cases_run[j]/p;
            file2 <<total_reco[j]/p<<","<<total_cases[j]/p;
            file2 << std::endl;
            }

            file2.close();

            */
            //*****************************************************************************************************************************
            std::ofstream file4;
            char fileName4[150];
            sprintf(fileName4,"./output/I_run_av_%d.data",p);
            file4.open(fileName4);

            for (int j=t0int;j<np;j++){


            file4 <<j<<","<<cases_run[j]/p;
            // file4 <<total_reco[j]/p<<","<<total_cases[j]/p;
            file4 << std::endl;

            }

            file4.close();

            std::ofstream file5;
            char fileName5[150];
            sprintf(fileName5,"./output/I_run_av_%d_fit1.data",p);
            file5.open(fileName5);

            for (int j=t0int;j<cutoff;j++){


            file5 <<j<<","<<cases_run[j]/p;
            // file5 <<total_reco[j]/p<<","<<total_cases[j]/p;
            file5 << std::endl;

            }

            file5.close();

            std::ofstream file6;
            char fileName6[150];
            sprintf(fileName6,"./output/I_run_av_%d_fit2.data",p);
            file6.open(fileName6);

            for (int j=t0int+cutoff;j<np;j++){


            file6 <<j<<","<<cases_run[j]/p;
            // file6 <<total_reco[j]/p<<","<<total_cases[j]/p;
            file6 << std::endl;

            }

            file6.close();

            betaGnuplotPrint();

          }
}

void deltaGen(int cutoff)
{
        std::ifstream file1("./scrapedData/I.data");

        int t0int = 0;


        //////////////////////////////////////////////////////////////////////////////////////////////////

        int val(0), rows(0), cols(0), numItems(0);
        while( file1.peek() != '\n' && file1 >> val )
        {
        std::cout << val << ' ';
        ++numItems;
        }
        cols = numItems;// # of columns found

        std::cout << '\n';
        while( file1 >> val )
        {
        ++numItems;
        std::cout << val << ' ';
        if( numItems%cols == 0 ) std::cout << '\n';
        }

        if( numItems > 0 )// got data!
        {
        rows = numItems/cols;
        std::cout << "rows = " << rows << ", cols = " << cols << '\n';
        }
        else// didn't get any data
        std::cout << "data reading failed\n";

        int num_days = rows;

        //////////////////////////////////////////////////////////////////////////////////////////////////





        std::ifstream file ("./scrapedData/Idot.data");
        std::ifstream file3("./scrapedData/I.data");
        if(file.is_open() && file3.is_open())
        {
        double  cases[num_days];
        double  reco[num_days];

        for(int i = 0; i < num_days; ++i)
        {
        file >> reco[i];
        file3>> cases[i];

        }






        int p  = 7;      //running average days
        int np = num_days-(p-1);

        int cases_run[np];
        int reco_run[np];
        int total_cases[num_days];
        int total_reco[num_days];

        for(int j=0; j<np; j++){
        cases_run[j]   = 0.0;
        reco_run[j]    = 0.0;
        total_cases[j] = 0.0;
        total_reco[j]  = 0.0;
        }





        for(int j=0; j<np;j++){
        for(int i=j+p-1; i>=j; i--){
        cases_run[j] =  cases_run[j] + cases[i] ;
        reco_run[j]  =   reco_run[j] + reco[i]  ;

        } }

        total_cases[0]  = cases_run[0] ;
        total_reco[0]   = reco_run[0]  ;

        for(int j=1; j<np; j++){
        if(j>=t0int){
        total_cases[j]  = total_cases[j-1]  + cases_run[j] ;
        total_reco[j]   = total_reco[j-1]   +  reco_run[j] ;
        }
        }



        //Printing

        //*****************************************************************************************************************************
        /*
        *  std::ofstream file2;
        char fileName1[150];
        sprintf(fileName1,"R1dot_I_run_av_%d_int.data",p);
        file2.open(fileName1);

        for (int j=t0int;j<np;j++){


        //file2 <<reco_run[j]/p<<","<<cases_run[j]/p;
        file2 <<total_reco[j]/p<<","<<total_cases[j]/p;
        file2 << std::endl;
        }

        file2.close();

        */
        //*****************************************************************************************************************************
        std::ofstream file4;
        char fileName4[150];
        sprintf(fileName4,"./output/I_run_av_%d.data",p);
        file4.open(fileName4);

        for (int j=t0int;j<np;j++){


        file4 <<j<<","<<cases_run[j]/p<<","<<reco_run[j]/p;
        // file4 <<total_reco[j]/p<<","<<total_cases[j]/p;
        file4 << std::endl;

        }

        file4.close();

        std::ofstream file5;
        char fileName5[150];
        sprintf(fileName5,"./output/I_run_av_%d_fit1.data",p);
        file5.open(fileName5);

        for (int j=t0int;j<np;j++){


        file5 <<j<<","<<cases_run[j]/p<<","<<reco_run[j]/p;
        // file5 <<total_reco[j]/p<<","<<total_cases[j]/p;
        file5 << std::endl;

        }

        file5.close();

        deltaGnuplotPrint();



        }
}

void gammaGen(int cutoff)
{
          std::ifstream file1("./scrapedData/R1dot.data");

          int t0int = 0;


          //////////////////////////////////////////////////////////////////////////////////////////////////

          int val(0), rows(0), cols(0), numItems(0);
          while( file1.peek() != '\n' && file1 >> val )
          {
          std::cout << val << ' ';
          ++numItems;
          }
          cols = numItems;// # of columns found

          std::cout << '\n';
          while( file1 >> val )
          {
          ++numItems;
          std::cout << val << ' ';
          if( numItems%cols == 0 ) std::cout << '\n';
          }

          if( numItems > 0 )// got data!
          {
          rows = numItems/cols;
          std::cout << "rows = " << rows << ", cols = " << cols << '\n';
          }
          else// didn't get any data
          std::cout << "data reading failed\n";

          int num_days = rows;

          //////////////////////////////////////////////////////////////////////////////////////////////////





          std::ifstream file ("./scrapedData/R1dot.data");
          std::ifstream file3("./scrapedData/I.data");
          std::ifstream file5 ("./scrapedData/Ddot.data");

          if(file.is_open() && file3.is_open() && file5.is_open())
          {
          double  cases[num_days];
          double  reco[num_days];
          double  reco1[num_days];

          for(int i = 0; i < num_days; ++i)
          {
          file >> reco[i];
          file5>> reco1[i];
          file3>> cases[i];

          }






          int p  = 7;      //running average days
          int np = num_days-(p-1);

          int cases_run[np];
          int reco_run[np];
          int reco1_run[np];

          int total_cases[num_days];
          int total_reco[num_days];
          int total_reco1[num_days];


          for(int j=0; j<np; j++){
          cases_run[j]   = 0.0;
          reco_run[j]    = 0.0;
          reco1_run[j]    = 0.0;

          total_cases[j] = 0.0;
          total_reco[j]  = 0.0;
          total_reco1[j]  = 0.0;

          }





          for(int j=0; j<np;j++){
          for(int i=j+p-1; i>=j; i--){
          cases_run[j] =  cases_run[j] + cases[i] ;
          reco_run[j]  =   reco_run[j] + reco[i]  ;
          reco1_run[j]  =   reco1_run[j] + reco1[i]  ;


          } }

          total_cases[0]  = cases_run[0] ;
          total_reco[0]   = reco_run[0]  ;
          total_reco1[0]   = reco1_run[0]  ;


          for(int j=1; j<np; j++){
          if(j>=t0int){
          total_cases[j]  = total_cases[j-1]  + cases_run[j] ;
          total_reco[j]   = total_reco[j-1]   +  reco_run[j] ;
          total_reco1[j]   = total_reco1[j-1]   +  reco1_run[j] ;

          }
          }



          //Printing

          //*****************************************************************************************************************************
          // std::ofstream file2;
          // char fileName1[150];
          // sprintf(fileName1,"./output/R1dot_I_run_av_%d_int.data",p);
          // file2.open(fileName1);

          std::ofstream fileNew;
          char fileNameNew[150];
          sprintf(fileNameNew,"./output/R1dot_I_run_av_%d_int2.data",p);
          fileNew.open(fileNameNew);

          for (int j=t0int;j<np;j++){


          //file2 <<reco_run[j]/p<<","<<cases_run[j]/p;
          // file2 <<(total_reco[j]+total_reco1[j])/p<<","<<total_cases[j]/p;
          fileNew <<(total_reco[j]+total_reco1[j])/p<<","<<total_cases[j]/p;

          fileNew << std::endl;
          // file2 << std::endl;
          }

          fileNew.close();
          // file2.close();


          //*****************************************************************************************************************************
          std::ofstream file4;
          char fileName4[150];
          sprintf(fileName4,"./output/R1dot_I_run_av_%d_int2_fit1.data",p);
          file4.open(fileName4);

          for (int j=t0int;j<cutoff;j++){


          file4 <<(total_reco[j]+total_reco1[j])/p<<","<<total_cases[j]/p;
          // file4 <<total_reco[j]/p<<","<<total_cases[j]/p;
          file4 << std::endl;

          }

          file4.close();

          std::ofstream fileN;
          char fileNameN[150];
          sprintf(fileNameN,"./output/R1dot_I_run_av_%d_int2_fit2.data",p);
          fileN.open(fileNameN);

          for (int j=100;j<130;j++){


          fileN <<(total_reco[j]+total_reco1[j])/p<<","<<total_cases[j]/p;
          // fileN <<total_reco[j]/p<<","<<total_cases[j]/p;
          fileN << std::endl;

          }

          fileN.close();

          gammaGnuplotPrint();

          }
}

void gammaDGen(int cutoff)
{
      std::ifstream file1("./scrapedData/R1dot.data");

      int t0int = 0;


      //////////////////////////////////////////////////////////////////////////////////////////////////

      int val(0), rows(0), cols(0), numItems(0);
      while( file1.peek() != '\n' && file1 >> val )
      {
      std::cout << val << ' ';
      ++numItems;
      }
      cols = numItems;// # of columns found

      std::cout << '\n';
      while( file1 >> val )
      {
      ++numItems;
      std::cout << val << ' ';
      if( numItems%cols == 0 ) std::cout << '\n';
      }

      if( numItems > 0 )// got data!
      {
      rows = numItems/cols;
      std::cout << "rows = " << rows << ", cols = " << cols << '\n';
      }
      else// didn't get any data
      std::cout << "data reading failed\n";

      int num_days = rows;

      //////////////////////////////////////////////////////////////////////////////////////////////////





      std::ifstream file ("./scrapedData/R1dot.data");
      std::ifstream file3("./scrapedData/I.data");
      std::ifstream file5 ("./scrapedData/Ddot.data");

      if(file.is_open() && file3.is_open() && file5.is_open())
      {
      double  cases[num_days];
      double  reco[num_days];
      double  reco1[num_days];

      for(int i = 0; i < num_days; ++i)
      {
      file >> reco[i];
      file5>> reco1[i];
      file3>> cases[i];

      }






      int p  = 7;      //running average days
      int np = num_days-(p-1);

      int cases_run[np];
      int reco_run[np];
      int reco1_run[np];

      int total_cases[num_days];
      int total_reco[num_days];
      int total_reco1[num_days];


      for(int j=0; j<np; j++){
      cases_run[j]   = 0.0;
      reco_run[j]    = 0.0;
      reco1_run[j]    = 0.0;

      total_cases[j] = 0.0;
      total_reco[j]  = 0.0;
      total_reco1[j]  = 0.0;

      }





      for(int j=0; j<np;j++){
      for(int i=j+p-1; i>=j; i--){
      cases_run[j] =  cases_run[j] + cases[i] ;
      reco_run[j]  =   reco_run[j] + reco[i]  ;
      reco1_run[j]  =   reco1_run[j] + reco1[i]  ;


      } }

      total_cases[0]  = cases_run[0] ;
      total_reco[0]   = reco_run[0]  ;
      total_reco1[0]   = reco1_run[0]  ;


      for(int j=1; j<np; j++){
      if(j>=t0int){
      total_cases[j]  = total_cases[j-1]  + cases_run[j] ;
      total_reco[j]   = total_reco[j-1]   +  reco_run[j] ;
      total_reco1[j]   = total_reco1[j-1]   +  reco1_run[j] ;

      }
      }



      //Printing

      //*****************************************************************************************************************************
      std::ofstream file2;
      char fileName1[150];
      sprintf(fileName1,"./output/R1dot_I_run_av_%d_int3.data",p);
      file2.open(fileName1);

      for (int j=t0int;j<np;j++){


      //file2 <<reco_run[j]/p<<","<<cases_run[j]/p;
      file2 <<(total_reco1[j])/p<<","<<total_cases[j]/p;
      file2 << std::endl;
      }

      file2.close();


      //*****************************************************************************************************************************
      std::ofstream file4;
      char fileName4[150];
      sprintf(fileName4,"./output/R1dot_I_run_av_%d_int3_fit1.data",p);
      file4.open(fileName4);

      for (int j=t0int+50;j<np;j++){


      file4 <<(total_reco1[j])/p<<","<<total_cases[j]/p;
      // file4 <<total_reco[j]/p<<","<<total_cases[j]/p;
      file4 << std::endl;

      }

      file4.close();

      gammaDGnuplotPrint();



      }
}

void gammaAGen(int cutoff)
{
    std::ifstream file1("./scrapedData/I.data");

    int t0int = 0;


    //////////////////////////////////////////////////////////////////////////////////////////////////

    int val(0), rows(0), cols(0), numItems(0);
    while( file1.peek() != '\n' && file1 >> val )
    {
    std::cout << val << ' ';
    ++numItems;
    }
    cols = numItems;// # of columns found

    std::cout << '\n';
    while( file1 >> val )
    {
    ++numItems;
    std::cout << val << ' ';
    if( numItems%cols == 0 ) std::cout << '\n';
    }

    if( numItems > 0 )// got data!
    {
    rows = numItems/cols;
    std::cout << "rows = " << rows << ", cols = " << cols << '\n';
    }
    else// didn't get any data
    std::cout << "data reading failed\n";

    int num_days = rows;

    //////////////////////////////////////////////////////////////////////////////////////////////////





    std::ifstream file ("./scrapedData/Idot.data");
    std::ifstream file3("./scrapedData/I.data");
    if(file.is_open() && file3.is_open())
    {
    double  cases[num_days];
    double  reco[num_days];

    for(int i = 0; i < num_days; ++i)
    {
    file >> reco[i];
    file3>> cases[i];

    }






    int p  = 7;      //running average days
    int np = num_days-(p-1);

    double cases_run[np];
    double reco_run[np];
    int total_cases[num_days];
    int total_reco[num_days];

    double dA[np];

    for(int j=0; j<np; j++){
    cases_run[j]   = 0.0;
    reco_run[j]    = 0.0;
    total_cases[j] = 0.0;
    total_reco[j]  = 0.0;
    }





    for(int j=0; j<np;j++){
    for(int i=j+p-1; i>=j; i--){
    cases_run[j] =  cases_run[j] + cases[i] ;
    reco_run[j]  =   reco_run[j] + reco[i]  ;

    } }

    total_cases[0]  = cases_run[0] ;
    total_reco[0]   = reco_run[0]  ;

    for(int j=1; j<np; j++){
    if(j>=t0int){
    total_cases[j]  = total_cases[j-1]  + cases_run[j] ;
    total_reco[j]   = total_reco[j-1]   +  reco_run[j] ;
    }
    }



    //Printing

    //*****************************************************************************************************************************
    /*
    *  std::ofstream file2;
    char fileName1[150];
    sprintf(fileName1,"R1dot_I_run_av_%d_int.data",p);
    file2.open(fileName1);

    for (int j=t0int;j<np;j++){


    //file2 <<reco_run[j]/p<<","<<cases_run[j]/p;
    file2 <<total_reco[j]/p<<","<<total_cases[j]/p;
    file2 << std::endl;
    }

    file2.close();

    */
    //*****************************************************************************************************************************
    std::ofstream file4;
    char fileName4[150];
    sprintf(fileName4,"./output/I_run_av_%d.data",p);
    file4.open(fileName4);

    double beta1 = 0.24;

    for (int j=t0int;j<np-1;j++){

    dA[j] = (reco_run[j]/p + 0.032*cases_run[j]/p) ;
    }


    for (int j=t0int;j<np-1;j++){

    double temp1 = log( dA[j] /  dA[0] ) ;

    double temp2(0.0);

    for (int i=t0int;i<j;i++){

    //                 if(i>30){
    //                     beta1 = 0.24/3.0;
    //                 }

    temp2 += beta1*( i + (0.05*cases_run[i]/p)/dA[i] );
    }

    file4 <<j<<","<<cases_run[j]/p<<","
       <<reco_run[j]/p<<","
       <<temp1<<","
       <<temp2;
    // file4 <<total_reco[j]/p<<","<<total_cases[j]/p;
    file4 << std::endl;

    }

    file4.close();

    gammaAGnuplotPrint();

    }
}

void webScrape(std::string city)
{
    FILE *pipe = popen("lwp-request https://api.covid19india.org/states_daily.json | json_xs > ./scrapedData/Delhi/output.json","r");
    pclose(pipe);

    boost::property_tree::ptree root;
    boost::property_tree::read_json("./scrapedData/Delhi/output.json", root);

    std::ofstream dlfile_I("./scrapedData/Delhi/Idot.data");
    std::ofstream dlfile_D("./scrapedData/Delhi/Ddot.data");
    std::ofstream dlfile_R("./scrapedData/Delhi/R1dot.data");

    // std::string myname = root.get<std::string> ("states_daily.second.kl", "NOT FOUND");
    std::vector<std::string> states_daily;
    for(boost::property_tree::ptree::value_type &states_dailyS : root.get_child("states_daily"))
    {
        states_daily.push_back(states_dailyS.second.data());
        if(states_dailyS.second.get<std::string> ("status", "NOT FOUND") == "Confirmed")
        {
            dlfile_I<<states_dailyS.second.get<std::string> (city, "DL NOT FOUND")<<std::endl;
        }

        if(states_dailyS.second.get<std::string> ("status", "NOT FOUND") == "Recovered")
        {
            dlfile_R<<states_dailyS.second.get<std::string> (city, "DL NOT FOUND")<<std::endl;
        }

        if(states_dailyS.second.get<std::string> ("status", "NOT FOUND") == "Deceased")
        {
            dlfile_D<<states_dailyS.second.get<std::string> (city, "DL NOT FOUND")<<std::endl;
        }
    }
}

void webScrapeDistrict(std::string city)
{
    FILE *pipe = popen("lwp-request https://api.covid19india.org/districts_daily.json | json_xs > ./scrapedData/outputdist.json","r");
    pclose(pipe);

    boost::property_tree::ptree root;
    boost::property_tree::read_json("./scrapedData/outputdist.json", root);

    std::ofstream dlfile_I("./scrapedData/Idot.data");
    std::ofstream dlfile_D("./scrapedData/Ddot.data");
    std::ofstream dlfile_R("./scrapedData/R1dot.data");

    // std::string myname = root.get<std::string> ("states_daily.second.kl", "NOT FOUND");
    std::vector<std::string> states_daily;
    for(boost::property_tree::ptree::value_type &states_dailyS : root.get_child("districtsDaily." + city))
    {
        states_daily.push_back(states_dailyS.second.data());

        dlfile_I<<states_dailyS.second.get<std::string> ("confirmed", "NOT FOUND")<<std::endl;
        dlfile_R<<states_dailyS.second.get<std::string> ("recovered", "NOT FOUND")<<std::endl;
        dlfile_D<<states_dailyS.second.get<std::string> ("deceased", "NOT FOUND")<<std::endl;
    }
}

void createIcumul()
{
            std::ifstream file1("./scrapedData/Delhi/Idot.data");

            int t0int = 0;


            //////////////////////////////////////////////////////////////////////////////////////////////////

            int val(0), rows(0), cols(0), numItems(0);
            while( file1.peek() != '\n' && file1 >> val )
            {
            std::cout << val << ' ';
            ++numItems;
            }
            cols = numItems;// # of columns found

            std::cout << '\n';
            while( file1 >> val )
            {
            ++numItems;
            std::cout << val << ' ';
            if( numItems%cols == 0 ) std::cout << '\n';
            }

            if( numItems > 0 )// got data!
            {
            rows = numItems/cols;
            std::cout << "rows = " << rows << ", cols = " << cols << '\n';
            }
            else// didn't get any data
            std::cout << "data reading failed\n";

            int num_days = rows;

            //////////////////////////////////////////////////////////////////////////////////////////////////



            std::ofstream fileOut("./scrapedData/Delhi/Ic.data");


            std::ifstream file3("./scrapedData/Delhi/Idot.data");
            if(file3.is_open())
            {
                double  Idot[num_days];

                for(int i = 0; i < num_days; ++i)
                {
                    //file >> reco[i];
                    file3>> Idot[i];
                    //std::cout<<"Idot: "<<i<<"\t"<<Idot[i]<<std::endl;

                }


                file3.close();
                double sumIc = 0.;
                for(int i = 0; i < num_days; ++i)
                {
                    sumIc += Idot[i];
                    fileOut<<sumIc<<std::endl;
                }

            //betaGnuplotPrint();

          }
}

void createDcumul()
{
            std::ifstream file1("./scrapedData/Delhi/Ddot.data");

            int t0int = 0;


            //////////////////////////////////////////////////////////////////////////////////////////////////

            int val(0), rows(0), cols(0), numItems(0);
            while( file1.peek() != '\n' && file1 >> val )
            {
            std::cout << val << ' ';
            ++numItems;
            }
            cols = numItems;// # of columns found

            std::cout << '\n';
            while( file1 >> val )
            {
            ++numItems;
            std::cout << val << ' ';
            if( numItems%cols == 0 ) std::cout << '\n';
            }

            if( numItems > 0 )// got data!
            {
            rows = numItems/cols;
            std::cout << "rows = " << rows << ", cols = " << cols << '\n';
            }
            else// didn't get any data
            std::cout << "data reading failed\n";

            int num_days = rows;

            //////////////////////////////////////////////////////////////////////////////////////////////////



            std::ofstream fileOut("./scrapedData/Delhi/Dc.data");


            std::ifstream file3("./scrapedData/Delhi/Ddot.data");
            if(file3.is_open())
            {
                double  Idot[num_days];

                for(int i = 0; i < num_days; ++i)
                {
                    //file >> reco[i];
                    file3>> Idot[i];
                    //std::cout<<"Idot: "<<i<<"\t"<<Idot[i]<<std::endl;

                }


                file3.close();
                double sumIc = 0.;
                for(int i = 0; i < num_days; ++i)
                {
                    sumIc += Idot[i];
                    fileOut<<sumIc<<std::endl;
                }

          }
}

void createRcumul()
{
            std::ifstream file1("./scrapedData/Delhi/R1dot.data");

            int t0int = 0;


            //////////////////////////////////////////////////////////////////////////////////////////////////

            int val(0), rows(0), cols(0), numItems(0);
            while( file1.peek() != '\n' && file1 >> val )
            {
            std::cout << val << ' ';
            ++numItems;
            }
            cols = numItems;// # of columns found

            std::cout << '\n';
            while( file1 >> val )
            {
            ++numItems;
            std::cout << val << ' ';
            if( numItems%cols == 0 ) std::cout << '\n';
            }

            if( numItems > 0 )// got data!
            {
            rows = numItems/cols;
            std::cout << "rows = " << rows << ", cols = " << cols << '\n';
            }
            else// didn't get any data
            std::cout << "data reading failed\n";

            int num_days = rows;

            //////////////////////////////////////////////////////////////////////////////////////////////////



            std::ofstream fileOut("./scrapedData/Delhi/Rc.data");


            std::ifstream file3("./scrapedData/Delhi/R1dot.data");
            if(file3.is_open())
            {
                double  Idot[num_days];

                for(int i = 0; i < num_days; ++i)
                {
                    //file >> reco[i];
                    file3>> Idot[i];
                    //std::cout<<"Idot: "<<i<<"\t"<<Idot[i]<<std::endl;

                }


                file3.close();
                double sumIc = 0.;
                for(int i = 0; i < num_days; ++i)
                {
                    sumIc += Idot[i];
                    fileOut<<sumIc<<std::endl;
                }

            //betaGnuplotPrint();

          }
}

void eulerDelhi(double t0, double t1, double t2, double S0, double A0, double I0, double R0, double dt, double t, double beta0, double beta1, double beta2, double gamma1, double gamma2, double gamma_D, double delta, double N, double phi)
{

    double S = S0;
    double I = I0;
    double A = A0;
    double R = R0;
    double D = 0 ;

    double Ic=I0;
    double AplusI_c = A0+I0;


    std::ofstream file;
    char fileName[150];
    sprintf(fileName,"./output/results/Del.data");
    file.open(fileName);


  	for(int i= 0; i<(t/dt);i++)
  	{
          double time = i*dt;

          //PRE Lock -----------------------------------------

          if(time<=t1){

      		S = S + dt*( -beta0*S*(A+I) ) ;
          A = A + dt*(  beta0*S*(A+I) - delta*A - gamma1*phi*A );
          I = I + dt*(   delta*A - gamma1*I );
          R = R + dt*(   gamma1*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;
          AplusI_c = AplusI_c + dt*(beta0*S*(A+I)) ;


          }
         //-------------------------------------------------------

          if(time>t1 && time<=t2){

          S = S + dt*( -beta1*S*(A+I) ) ;
          A = A + dt*(  beta1*S*(A+I) - delta*A - gamma2*phi*A );
          I = I + dt*(   delta*A - gamma2*I );
          R = R + dt*(   gamma2*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;
          AplusI_c = AplusI_c + dt*(beta1*S*(A+I)) ;

          }

          if(time>t2){

          S = S + dt*( -beta2*S*(A+I) ) ;
          A = A + dt*(  beta2*S*(A+I) - delta*A - gamma2*phi*A );
          I = I + dt*(   delta*A - gamma2*I );
          R = R + dt*(   gamma2*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;
          AplusI_c = AplusI_c + dt*(beta2*S*(A+I)) ;

          }

          else{}

          if(i%100==0){

            file   << time<<","<<N*S<<","<<N*A<<","<<N*I<<","<<N*R<<","<<Ic*N<<","<<D*N<<","<<delta*A*N<<","<<gamma_D*I*N<<","<<AplusI_c*N;
            //     file   << time<<","<<Ic*N;
            file<<std::endl;
          }

  	}
  	file.close();


}

void eulerMumbai(double t0, double t1, double t2, double S0, double A0, double I0, double R0, double dt, double t, double beta0, double beta1, double beta2, double gamma1, double gamma2, double gamma_D, double delta, double N, double phi)
{

    double S = S0;
    double I = I0;
    double A = A0;
    double R = R0;
    double D = 0 ;

    double Ic=I0;
    double AplusI_c = A0+I0;



    std::ofstream file;
    char fileName[150];
    sprintf(fileName,"./output/results/Mum.data");
    file.open(fileName);


  	for(int i= 0; i<(t/dt);i++)
  	{
          double time = i*dt;

          //PRE Lock -----------------------------------------

          if(time<=t1){

  		S = S + dt*( -beta0*S*(A+I) ) ;
          A = A + dt*(  beta0*S*(A+I) - delta*A - gamma1*phi*A );
          I = I + dt*(   delta*A - gamma1*I );
          R = R + dt*(   gamma1*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;
          AplusI_c = AplusI_c + dt*(beta0*S*(A+I)) ;



          }
         //-------------------------------------------------------

          if(time>t1 && time<=t2){

          S = S + dt*( -beta1*S*(A+I) ) ;
          A = A + dt*(  beta1*S*(A+I) - delta*A - gamma2*phi*A );
          I = I + dt*(   delta*A - gamma2*I );
          R = R + dt*(   gamma2*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;
          AplusI_c = AplusI_c + dt*(beta1*S*(A+I)) ;


          }

          if(time>t2){

          S = S + dt*( -beta2*S*(A+I) ) ;
          A = A + dt*(  beta2*S*(A+I) - delta*A - gamma2*phi*A );
          I = I + dt*(   delta*A - gamma2*I );
          R = R + dt*(   gamma2*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;
          AplusI_c = AplusI_c + dt*(beta2*S*(A+I)) ;



          }

          else{}

          if(i%100==0){

            file   << time<<","<<N*S<<","<<N*A<<","<<N*I<<","<<N*R<<","<<Ic*N<<","<<D*N<<","<<delta*A*N<<","<<gamma_D*I*N<<","<<AplusI_c*N;
            //     file   << time<<","<<Ic*N ;

            file<<std::endl;
          }

  	}
	  file.close();


}

void eulerBengaluru(double t0, double t1, double t2, double S0, double A0, double I0, double R0, double dt, double t, double beta0, double beta1, double beta2, double gamma1, double gamma2, double gamma_D, double delta, double N, double phi)
{

    double S = S0;
    double I = I0;
    double A = A0;
    double R = R0;
    double D = 0 ;

    double Ic=I0;
    double AplusI = A0+I0;


    std::ofstream file;
    char fileName[150];
    sprintf(fileName,"./output/results/Blr.data");
    file.open(fileName);


  	for(int i= 0; i<(t/dt);i++)
  	{
          double time = i*dt;

          //PRE Lock -----------------------------------------

          if(time<=t1){

  		S = S + dt*( -beta0*S*(A+I) ) ;
          A = A + dt*(  beta0*S*(A+I) - delta*A - gamma1*phi*A );
          I = I + dt*(   delta*A - gamma1*I );
          R = R + dt*(   gamma1*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;


          }
         //-------------------------------------------------------

          if(time>t1 && time<=t2){

          S = S + dt*( -beta1*S*(A+I) ) ;
          A = A + dt*(  beta1*S*(A+I) - delta*A - gamma2*phi*A );
          I = I + dt*(   delta*A - gamma2*I );
          R = R + dt*(   gamma2*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;

          }

          if(time>t2){

          S = S + dt*( -beta2*S*(A+I) ) ;
          A = A + dt*(  beta2*S*(A+I) - delta*A - gamma2*phi*A );
          I = I + dt*(   delta*A - gamma2*I );
          R = R + dt*(   gamma2*( phi*A + I));
          D = D + dt*( gamma_D*I);
          Ic= Ic+ dt*( delta*A)  ;


          }

          else{}

          if(i%1500==0){

          file   << time<<","<<N*S<<","<<N*A<<","<<N*I<<","<<N*R<<","<<Ic*N<<","<<D*N ;
          file<<std::endl;
          }

  	}
	  file.close();


}

void numericalCalcDelhi()
{
    double t0 = 0, dt = 0.01, t = 400, t1 = 43, t2 = 117;
    double  N    = 1.8e7;
    double  I0   = 1/N;                // 1 in a million infection
    double  A0   = 1*I0;
    double  S0   = (1.0 - (A0+I0));
    double  R0   = 0.0;


    double beta0 = 0.26;
    double beta1 = 0.14;
    double beta2 = 0.09;

    double gamma1  = 0.039;
    double gamma2  = 0.1;

    double delta  = 0.01;

    double gamma_D= 0.004;

    double phi = 1.0;


    eulerDelhi (t0, t1, t2, S0, A0, I0, R0, dt, t, beta0, beta1, beta2, gamma1, gamma2, gamma_D, delta, N, phi); 
}

void numericalCalcBengaluru()
{
    double t0 = 0, dt = 0.01, t = 600, t1 = 70, t2 = 100;
    double  N    = 1.23e7;
    double  I0   = 1/N;                // 1 in a million infection
    double  A0   = 1*I0;
    double  S0   = (1.0 - (A0+I0));
    double  R0   = 0.0;


    double beta0 = 0.05;
    double beta1 = 0.18;
    double beta2 = beta1;

    double gamma1  = 0.034;
    double gamma2  = gamma1;

    double delta  = 0.0006;

    double gamma_D= 0.002;

    double phi = 1.0;


    eulerBengaluru (t0, t1, t2, S0, A0, I0, R0, dt, t, beta0, beta1, beta2, gamma1, gamma2, gamma_D, delta, N, phi);
}

void numericalCalcMumbai()
{
    double t0 = 0, dt = 0.01, t = 400, t1 = 40, t2 = 70;
    double  N    = 1.24e7;
    double  I0   = 1/N;                // 1 in a million infection
    double  A0   = 1*I0;
    double  S0   = (1.0 - (A0+I0));
    double  R0   = 0.0;


    double beta0 = 0.3;
    double beta1 = 0.12;//0.09;
    double beta2 = 0.09;

    double gamma1  = 0.029;
    double gamma2  = 0.07;

    double delta  = 0.0035;

    double gamma_D= 0.003;

    double phi = 1.0;


    eulerMumbai (t0, t1, t2, S0, A0, I0, R0, dt, t, beta0, beta1, beta2, gamma1, gamma2, gamma_D, delta, N, phi);
}

void createIdata()
{
            std::ifstream file1("./scrapedData/Delhi/Ic.data");

            int t0int = 0;


            //////////////////////////////////////////////////////////////////////////////////////////////////

            int val(0), rows(0), cols(0), numItems(0);
            while( file1.peek() != '\n' && file1 >> val )
            {
            std::cout << val << ' ';
            ++numItems;
            }
            cols = numItems;// # of columns found

            std::cout << '\n';
            while( file1 >> val )
            {
            ++numItems;
            std::cout << val << ' ';
            if( numItems%cols == 0 ) std::cout << '\n';
            }

            if( numItems > 0 )// got data!
            {
            rows = numItems/cols;
            std::cout << "rows = " << rows << ", cols = " << cols << '\n';
            }
            else// didn't get any data
            std::cout << "data reading failed\n";

            int num_days = rows;

            //////////////////////////////////////////////////////////////////////////////////////////////////


            std::ifstream file3("./scrapedData/Delhi/Rc.data");
            std::ifstream file4("./scrapedData/Delhi/Dc.data");
            std::ifstream file5("./scrapedData/Delhi/Ic.data");


            std::ofstream fileOut("./scrapedData/Delhi/I.data");

            if(file3.is_open() && file4.is_open() && file5.is_open())
            {
                double Ic[num_days];
                double Dc[num_days];
                double Rc[num_days];

                for(int i = 0; i < num_days; ++i)
                {
                    file3>> Rc[i];
                    file4>> Dc[i];
                    file5>> Ic[i];

                    fileOut<<(Ic[i] - Dc[i] - Rc[i])<<std::endl;
                }


                file3.close();
                file4.close();
                file5.close();
            }
}

void extractParams()
{
  createIcumul();
  createRcumul();
  createDcumul();
  createIdata();
}

/*********************************************************************************************
 *   Copyright (c) <2020>, <Santosh Ansumali@JNCASR>                                         *
 *   All rights reserved.                                                                    *
 *   Redistribution and use in source and binary forms, with or without modification, are    *
 *   permitted provided that the following conditions are met:                               *
 *                                                                                           *
 *    1. Redistributions of source code must retain the above copyright notice, this list of *
 *       conditions and the following disclaimer.                                            *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list *
 *       of conditions and the following disclaimer in the documentation and/or other        *
 *       materials provided with the distribution.                                           *
 *    3. Neither the name of the <JNCASR> nor the names of its contributors may be used to   *
 *       endorse or promote products derived from this software without specific prior       *
 *       written permission.                                                                 *
 *                                                                                           *
 *       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND     *
 *       ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED       *
 *       WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  *
 *       IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,    *
 *       INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,      *
 *       BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,       *
 *       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 *       LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE     *
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED   *
 *       OF THE POSSIBILITY OF SUCH DAMAGE.                                                  *
 *                                                                                           *
 *       Suggestions:          ansumali@jncasr.ac.in                                         *
 *       Bugs:                 ansumali@jncasr.ac.in                                         *
 *                                                                                           *
 *********************************************************************************************/

/*********************************************************************************************

 *  @Authors: Shaurya Kaushal, Akshay Chandran and Santosh Ansumali                                            *

 *********************************************************************************************/

#include "model.h"

void medInventory()
{
    double N = 1.84e07;
    double I0 = 10.;


    std::ifstream file1("./scrapedData/Mum_1.data");
    // ifstream file2("./inputData/Del_1.data");

    // fractions
    double fs = 0.4;
    double fc = 0.05;
    double fa = 0.15;
    double fd = 0.025;

    // relaxation times
    double tauc = 7.;
    double taucInv = 1./tauc;
    double taud = 9.;
    double taudInv = 1./taud;
    double taus = 14.;
    double tausInv = 1./taus;
    double taua = 14.;
    double tauaInv = 1./taua;


    //////////////////////////////////////////////////////////////////////////////////////////////////

    double val(0);
    int rows(0), cols(0), numItems(0);
    while( file1.peek() != '\n' && file1 >> val )
    {
        std::cout << val << ' ';
        ++numItems;
    }
    cols = numItems;// # of columns found

    std::cout << '\n';
    while( file1 >> val )
    {
        ++numItems;
        // std::cout << val << ' ';
        // if( numItems%cols == 0 ) std::cout << '\n';
    }

    if( numItems > 0 )// got data!
    {
        rows = numItems/cols;
        std::cout << "rows = " << rows << ", cols = " << cols << '\n';
    }
    else// didn't get any data
        std::cout << "data reading failed\n";

    int num_days = rows;

    //////////////////////////////////////////////////////////////////////////////////////////////////

    double I2[num_days];
    double I[num_days];
    cout<<"num_days: "<<num_days<<endl;

    std::ifstream file3("./scrapedData/Mum_1.data");
    std::ofstream file5("./scrapedData/Mum_1out.data");

    if(file3.is_open())
    {
        for(int i = 0; i < num_days; i++)
        {
            file3 >> I2[i];
        }

        I[0] = I2[0];
        for(int i = 1; i < num_days; i++)
        {
            I[i] = I2[i] - I2[i - 1];
            file5 <<i*.01<<"\t"<<I2[i] - I2[i - 100]<<endl;
        }

        file3.close();
    }

    std::ofstream fileMed("./output/results/medFile.dat");

    int t0 = 0;
    int tf = 40000;
    int tn = 1;
    double C = fc*I[0];
    double A = fa*I[0];
    double S = fs*I[0];

    while(tn < tf)
    {
      C += (fc*I[tn] - (1. - fd)*C*taucInv + fd*C*taudInv)*0.01;
      A += (fa*I[tn] - A*tauaInv)*0.01;
      S += (fs*I[tn] - S*tausInv + (1. - fd)*C*taucInv)*0.01;

      fileMed<<tn*0.01<<"\t"<<C<<"\t"<<A<<"\t"<<S<<endl;

      tn++;
    }
}
